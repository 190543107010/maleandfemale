package com.example.malefemalegenerater;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.malefemalegenerater.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    EditText fname,email,lname;
    Button btn;
    RadioButton rb1m,rb2f;
    RadioGroup rg;
    CheckBox chkcri,chkhok,chkfoot;

    ArrayList<HashMap<String,Object>> userlist = new ArrayList<>();
    boolean doubleBackToExitPressedOnce = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        idrefreance();
        intevent();
    }
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    void idrefreance()
    {
        fname = findViewById(R.id.edfname);
        lname = findViewById(R.id.edlname);
        email = findViewById(R.id.edemail);
        btn = findViewById(R.id.btnsubmit);
        rb1m = findViewById(R.id.rb_male);
        rb2f = findViewById( R.id.rb_female);
        rg = findViewById(R.id.rd_gp);
        chkcri = findViewById(R.id.chk_cricket);
        chkfoot = findViewById(R.id.chk_footboll);
        chkhok = findViewById(R.id.chl_hokey);
    }
    void intevent()
    {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isvalids()) {
                    HashMap<String, Object> map = new HashMap<>();
                    map.put(Const.first_name, fname.getText().toString());
                    map.put(Const.last_name, lname.getText().toString());
                    map.put(Const.email, email.getText().toString());
                    map.put(Const.gender, rb1m.isChecked() ? rb1m.getText().toString() : rb2f.getText().toString());

                    String hobbies = "";
                    if (chkcri.isChecked()) {
                        hobbies += "," + chkcri.getText().toString();
                    }
                    if (chkhok.isChecked()) {
                        hobbies += "," + chkhok.getText().toString();
                    }
                    if (chkfoot.isChecked()) {
                        hobbies += "," + chkfoot.getText().toString();
                    }
                    if (hobbies.length() > 0) {
                        hobbies.substring(1);
                    }
                    map.put(Const.hobby, hobbies);
                    int rid = rg.getCheckedRadioButtonId();
                    View rb = rg.findViewById(rid);
                    int idx = rg.indexOfChild(rb);
                    String gn = "";
                    if (idx == 1) {
                        map.put(Const.gender, gn = "m");
                    } else {
                        map.put(Const.gender, gn = "f");
                    }

                    userlist.add(map);

                    Intent i = new Intent(MainActivity.this, second.class);
                    i.putExtra("userlst", userlist);
                    startActivity(i);
                    Toast.makeText(getApplicationContext(), rb1m.isChecked() ? "male" : "female", Toast.LENGTH_SHORT).show();
                }
            }
        });
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if(i == R.id.rb_male)
                {
                    chkcri.setVisibility(View.VISIBLE);
                    chkfoot.setVisibility(View.VISIBLE);chkhok.setVisibility(View.VISIBLE);
                }
                else
                {
                    chkhok.setVisibility(View.VISIBLE);
                    chkcri.setVisibility(View.VISIBLE);
                    chkfoot.setVisibility(View.GONE);
                }
            }
        });

    }
    boolean isvalids()
    {
        boolean flag = true;
        if(TextUtils.isEmpty(fname.getText()))
        {
            fname.setError("enter First name");
            flag = false;
        }
        if(TextUtils.isEmpty(lname.getText()))
        {
            lname.setError("enter Last name");
            flag = false;
        }

        if(TextUtils.isEmpty(email.getText()))
        {
            email.setError("enter email");
            flag = false;
        }
        else
        {
            String emil = email.getText().toString().trim();
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if(!emil.matches(emailPattern))
            {
                email.setError("email is not valid");
                flag = false;
            }
        }
        return flag;
    }

}
