package com.example.malefemalegenerater.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.malefemalegenerater.R;
import com.example.malefemalegenerater.util.Const;

import java.util.ArrayList;
import java.util.HashMap;

public class userlistadapter extends BaseAdapter {

    Context context;
    ArrayList<HashMap<String,Object>> usrlist;

    public userlistadapter(Context context, ArrayList<HashMap<String,Object>> usrlist) {
        this.context = context;
        this.usrlist = usrlist;
    }

    @Override
    public int getCount() {
        return usrlist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View view1 = LayoutInflater.from(context).inflate(R.layout.view_row_user,null);
        TextView tvfnm = view1.findViewById(R.id.tvname);
        TextView tveml = view1.findViewById(R.id.tvemaile);
        TextView tvgen = view1.findViewById(R.id.tvlvtgender);

        tvfnm.setText(usrlist.get(position).get(Const.first_name)+" "+usrlist.get(position).get(Const.last_name));
        tveml.setText(String.valueOf(usrlist.get(position).get(Const.email)));
        tvgen.setText(String.valueOf(usrlist.get(position).get(Const.gender)));
        String gn =tvgen.getText().toString();
        if( gn.equals("m"))
        {
            tvgen.setBackgroundResource(R.drawable.ic_male_background);
        }
        else
        {
            tvgen.setBackgroundResource(R.drawable.ic_female_background);
        }

        return view1;
    }
}
